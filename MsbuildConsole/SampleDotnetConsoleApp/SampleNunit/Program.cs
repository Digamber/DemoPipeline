﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleNunit
{
    public class Program
    {
        public static void Main()
        {
            Console.Write("1つめの数字を入力：");
            int x = int.Parse(Console.ReadLine());

            Console.Write("2つめの数字を入力：");
            int y = int.Parse(Console.ReadLine());

            int result = Add(x, y);

            Console.WriteLine("計算結果は" + result + "です。");
            Console.WriteLine("何かキーを押してください。");
            Console.ReadKey();

        }

        public static int Add(int x, int y)
        {
            return x + y;
        }
    }
}
