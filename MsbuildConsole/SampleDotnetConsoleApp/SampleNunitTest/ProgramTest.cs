﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SampleNunitTest
{
    [TestFixture]
    public class ProgramTest
    {
        [Test]
        public void AddTest()
        {
            int x = 100;
            int y = 200;
            int exp = 300;
            int act;

            act = SampleNunit.Program.Add(x, y);
            Assert.AreEqual(exp, act);
        }
    }
}
